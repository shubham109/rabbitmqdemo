var amqp = require('amqplib/callback_api');
amqp.connect('amqp://localhost',(err,connection)=>{
    if(err){
        throw err;
    }
connection.createChannel((err,channel)=>{
    if(err) throw err;
    var queue = 'hello'
    channel.assertQueue(queue,{durable:false})
    channel.consume(queue,(msg)=>{
        console.log("[x] received %s ",msg.content.toString());
    },{
        noAcke:true
    })
})
})